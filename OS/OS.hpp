#ifndef OS_HPP_
#define OS_HPP_

#include "FreeRTOS.h"
#include "task.h"

class OS
{
public:
	static TaskHandle_t addTask(void (*f)(void*), const char* name, uint16_t stackSize, void* params, uint8_t prio);
	static void startScheduler();
private:
	static bool _is_OS_turned_on;
	static void incrementTick();
};


#endif
