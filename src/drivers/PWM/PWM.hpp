#ifndef DRIVERS_PWM_PWM_HPP_
#define DRIVERS_PWM_PWM_HPP_

#include "Driver.hpp"
#include "stm32f1xx_hal.h"

enum PWMType
{
	TIM1_CH1,
	TIM1_CH2,
	TIM1_CH3,
	TIM2_CH2_4,
	LAST
};

class PWM : private Driver
{
public:
	PWM(PWMType type);
	~PWM();

	void transmitStream(void* data, uint32_t dataLen);
private:
	TIM_HandleTypeDef* htim;
	DMA_HandleTypeDef* hdma;
	uint32_t channel;
	PWMType type;
	static uint8_t TIM1InitCount;
};


#endif
