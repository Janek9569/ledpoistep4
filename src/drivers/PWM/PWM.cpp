#include "PWM.hpp"
#include "string.h"
#include "global_utils.h"

static TIM_HandleTypeDef htim1;
static TIM_HandleTypeDef htim2;
static DMA_HandleTypeDef hdma_tim1_ch1;
static DMA_HandleTypeDef hdma_tim1_ch2;
static DMA_HandleTypeDef hdma_tim1_ch3;
static DMA_HandleTypeDef hdma_tim2_ch2_ch4;

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(timHandle->Instance==TIM1)
	{
		/* USER CODE END TIM1_MspPostInit 0 */
		/**TIM1 GPIO Configuration
		PA8     ------> TIM1_CH1
		PA9     ------> TIM1_CH2
		PA10     ------> TIM1_CH3
		*/
		GPIO_InitStruct.Pin = GP(8) | GP(9) | GP(10);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
	else if(timHandle->Instance==TIM2)
	{
		/**TIM2 GPIO Configuration
		PB11     ------> TIM2_CH4
		*/
		GPIO_InitStruct.Pin = GP(11);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		__HAL_AFIO_REMAP_TIM2_PARTIAL_2();
	}
}

void HAL_TIM_PWM_MspDeInit(TIM_HandleTypeDef* tim_pwmHandle)
{
	if(tim_pwmHandle->Instance==TIM1)
	{
		__HAL_RCC_TIM1_CLK_DISABLE();

		HAL_DMA_DeInit(tim_pwmHandle->hdma[TIM_DMA_ID_CC1]);
		HAL_DMA_DeInit(tim_pwmHandle->hdma[TIM_DMA_ID_CC2]);
		HAL_DMA_DeInit(tim_pwmHandle->hdma[TIM_DMA_ID_CC3]);
	}
	else if(tim_pwmHandle->Instance==TIM2)
	{
		__HAL_RCC_TIM2_CLK_DISABLE();

		HAL_DMA_DeInit(tim_pwmHandle->hdma[TIM_DMA_ID_CC2]);
		HAL_DMA_DeInit(tim_pwmHandle->hdma[TIM_DMA_ID_CC4]);
	}
}

void MX_TIM1_Init(void)
{
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_OC_InitTypeDef sConfigOC;
	TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

	htim1.Instance = TIM1;
	htim1.Init.Prescaler = 0;
	htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim1.Init.Period = 89;
	htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim1.Init.RepetitionCounter = 0;
	htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	ASSERT(HAL_TIM_PWM_Init(&htim1) == HAL_OK);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	ASSERT(HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) == HAL_OK);

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
	sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
	ASSERT(HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) == HAL_OK);
	ASSERT(HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) == HAL_OK);
	ASSERT(HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) == HAL_OK);

	sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
	sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
	sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
	sBreakDeadTimeConfig.DeadTime = 0;
	sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
	sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
	sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
	ASSERT(HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) == HAL_OK);
	HAL_TIM_MspPostInit(&htim1);
}

void MX_TIM2_Init(void)
{
	TIM_MasterConfigTypeDef sMasterConfig;
	TIM_OC_InitTypeDef sConfigOC;

	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 0;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 89;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	ASSERT(HAL_TIM_PWM_Init(&htim2) == HAL_OK);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	ASSERT(HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) == HAL_OK);

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	ASSERT(HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) == HAL_OK);

	HAL_TIM_MspPostInit(&htim2);
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* tim_pwmHandle)
{
	if(tim_pwmHandle->Instance==TIM1)
	{
		__HAL_RCC_TIM1_CLK_ENABLE();

		hdma_tim1_ch1.Instance = DMA1_Channel2;
		hdma_tim1_ch1.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_tim1_ch1.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_tim1_ch1.Init.MemInc = DMA_MINC_ENABLE;
		hdma_tim1_ch1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_tim1_ch1.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_tim1_ch1.Init.Mode = DMA_CIRCULAR;
		hdma_tim1_ch1.Init.Priority = DMA_PRIORITY_LOW;
		ASSERT(HAL_DMA_Init(&hdma_tim1_ch1) == HAL_OK);
		__HAL_LINKDMA(tim_pwmHandle,hdma[TIM_DMA_ID_CC1],hdma_tim1_ch1);

		hdma_tim1_ch2.Instance = DMA1_Channel3;
		hdma_tim1_ch2.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_tim1_ch2.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_tim1_ch2.Init.MemInc = DMA_MINC_ENABLE;
		hdma_tim1_ch2.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_tim1_ch2.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_tim1_ch2.Init.Mode = DMA_CIRCULAR;
		hdma_tim1_ch2.Init.Priority = DMA_PRIORITY_LOW;
		ASSERT(HAL_DMA_Init(&hdma_tim1_ch2) == HAL_OK);
		__HAL_LINKDMA(tim_pwmHandle,hdma[TIM_DMA_ID_CC2],hdma_tim1_ch2);

		hdma_tim1_ch3.Instance = DMA1_Channel6;
		hdma_tim1_ch3.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_tim1_ch3.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_tim1_ch3.Init.MemInc = DMA_MINC_ENABLE;
		hdma_tim1_ch3.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_tim1_ch3.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_tim1_ch3.Init.Mode = DMA_CIRCULAR;
		hdma_tim1_ch3.Init.Priority = DMA_PRIORITY_LOW;
		ASSERT(HAL_DMA_Init(&hdma_tim1_ch3) == HAL_OK);
		__HAL_LINKDMA(tim_pwmHandle,hdma[TIM_DMA_ID_CC3],hdma_tim1_ch3);
	}
	else if(tim_pwmHandle->Instance==TIM2)
	{
		__HAL_RCC_TIM2_CLK_ENABLE();

		hdma_tim2_ch2_ch4.Instance = DMA1_Channel7;
		hdma_tim2_ch2_ch4.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_tim2_ch2_ch4.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_tim2_ch2_ch4.Init.MemInc = DMA_MINC_ENABLE;
		hdma_tim2_ch2_ch4.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_tim2_ch2_ch4.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_tim2_ch2_ch4.Init.Mode = DMA_CIRCULAR;
		hdma_tim2_ch2_ch4.Init.Priority = DMA_PRIORITY_LOW;
		ASSERT(HAL_DMA_Init(&hdma_tim2_ch2_ch4) == HAL_OK);

		__HAL_LINKDMA(tim_pwmHandle,hdma[TIM_DMA_ID_CC2],hdma_tim2_ch2_ch4);
		__HAL_LINKDMA(tim_pwmHandle,hdma[TIM_DMA_ID_CC4],hdma_tim2_ch2_ch4);
	}
}

PWM::PWM(PWMType type)
{
	static bool firstEnter = 1;
	this->type = type;

	if(firstEnter)
	{
		attachToISR(Driver::ISR::DMA1_CH2, [this]()->void {
			HAL_DMA_IRQHandler(&hdma_tim1_ch1);
			releaseSemISR();
		});

		attachToISR(Driver::ISR::DMA1_CH3, [this]()->void {
			HAL_DMA_IRQHandler(&hdma_tim1_ch2);
			releaseSemISR();
		});

		attachToISR(Driver::ISR::DMA1_CH6, [this]()->void {
			HAL_DMA_IRQHandler(&hdma_tim1_ch3);
			releaseSemISR();
		});

		attachToISR(Driver::ISR::DMA1_CH7, [this]()->void {
			HAL_TIM_IRQHandler(&htim2);
			releaseSemISR();
		});

		MX_TIM1_Init();
		MX_TIM2_Init();

		HAL_TIM_Base_Start(&htim1);
		HAL_TIM_Base_Start(&htim2);

		firstEnter = 0;
	}

	switch(type)
	{
	case TIM1_CH1:
		TIM1->CCR1 = 0;
		channel = TIM_CHANNEL_1;
		hdma = &hdma_tim1_ch1;
		htim = &htim1;
		break;
	case TIM1_CH2:
		channel = TIM_CHANNEL_2;
		TIM1->CCR2 = 0;
		hdma = &hdma_tim1_ch2;
		htim = &htim1;
		break;
	case TIM1_CH3:
		TIM1->CCR3 = 0;
		channel = TIM_CHANNEL_3;
		hdma = &hdma_tim1_ch3;
		htim = &htim1;
		break;
	case TIM2_CH2_4:
		TIM2->CCR2 = 0;
		TIM2->CCR4 = 0;
	    channel = TIM_CHANNEL_4;
	    hdma = &hdma_tim2_ch2_ch4;
	    htim = &htim2;
		break;
	default:
		ASSERT(3==8);
	}

	releaseSem();
}

PWM::~PWM()
{
}

void PWM::transmitStream(void* data, uint32_t dataLen)
{
	lockSem(WAIT_FOR_EVER);
	HAL_TIM_PWM_Start_DMA(htim, channel, (uint32_t*) data, dataLen);
}

