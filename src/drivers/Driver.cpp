#include <Driver.hpp>
#include "global_utils.h"
#include "stm32f1xx_hal.h"

std::function<void(void)> Driver::ISRHandler[ISR_LAST];

Driver::Driver()
{
	static bool isFirstDriver = true;
	if(isFirstDriver)
	{
		__HAL_RCC_DMA1_CLK_ENABLE();

		/* SPI2_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(SPI2_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(SPI2_IRQn);
		/* SPI1_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(SPI1_IRQn);
		/* TIM2_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM2_IRQn);
		/* EXTI15_10_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		/* DMA1_Channel7_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
		/* DMA1_Channel6_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
		/* DMA1_Channel5_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
		/* DMA1_Channel4_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
		/* DMA1_Channel3_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
		/* DMA1_Channel2_IRQn interrupt configuration */
		HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
		isFirstDriver = false;
	}
	sem = xSemaphoreCreateBinary();
}

Driver::~Driver()
{

}

void Driver::releaseSemISR()
{
	BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(sem, &xHigherPriorityTaskWoken);
}

void Driver::attachToISR(volatile ISR isr, std::function<void(void)> f)
{
	ISRHandler[isr] = f;
}

void Driver::callISRHandler(volatile ISR isr)
{
	if(ISRHandler[isr])
		ISRHandler[isr]();
}

/*****INTS*****/
CEXT void DMA1_Channel5_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH5);
}

CEXT void DMA1_Channel4_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH4);
}

CEXT void DMA1_Channel2_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH2);
}

CEXT void DMA1_Channel3_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH3);
}

CEXT void DMA1_Channel6_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH6);
}

CEXT void DMA1_Channel7_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH7);
}

CEXT void SPI1_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::SPI1_IRQ);
}

CEXT void SPI2_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::SPI2_IRQ);
}

CEXT void TIM2_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::TIM2_IRQ);
}

CEXT void EXTI15_10_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI15_10_IRQn 0 */

  /* USER CODE END EXTI15_10_IRQn 0 */
  //HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
  /* USER CODE BEGIN EXTI15_10_IRQn 1 */

  /* USER CODE END EXTI15_10_IRQn 1 */
}
