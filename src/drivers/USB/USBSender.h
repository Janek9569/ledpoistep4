#ifndef USBSENDER_H_
#define USBSENDER_H_

#include <cstring>
#include "global_utils.h"
#include "FreeRTOS.h"
#include "queue.h"

class USBSender
{
public:

	class USBPacket
	{
	public:
		USBPacket(const USBPacket&) = delete;
		USBPacket& operator=(USBPacket& rhs) = delete;

		//\0 terminated
		USBPacket(uint8_t* ptr, uint32_t size)
		{
			ASSERT(size < maxDataSize);
			rawData = (uint8_t*) pvPortMalloc(size);
			ASSERT(rawData != NULL);
			memset(rawData, 0x00, size);

			memcpy(rawData, ptr, size);
		}

		void *operator new(size_t size)
		{
			void *p = pvPortMalloc(size);
			return p;
		}

		void operator delete(void *p)
		{
			vPortFree(p);
		}

		size_t getSize() const
		{
			return strlen((const char*)rawData);
		}

		uint8_t* getData() const
		{
			return rawData;
		}

		~USBPacket()
		{
			vPortFree(rawData);
		}
		static constexpr uint32_t maxDataSize = 50;
	private:

		uint8_t* rawData;
	};

	USBSender();
	~USBSender();

	bool pushData(USBPacket* packet);
private:
	static void usbSender_deamon(void* argument);
	QueueHandle_t msgQ;
};



#endif
