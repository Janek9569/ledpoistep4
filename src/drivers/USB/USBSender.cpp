#include "USBSender.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "OS.hpp"
#include "usb_device.h"

void USBSender::usbSender_deamon(void* argument)
{
	USBSender* usbSender = (USBSender*)argument;
	USBPacket* packet = nullptr;
	MX_USB_DEVICE_Init();

	while( xQueueReceive( usbSender->msgQ, (void*)&(packet), (TickType_t)WAIT_FOR_EVER) )
	{
		ASSERT(packet != nullptr);

		CDC_Transmit_FS((uint8_t*)packet->getData(), packet->getSize());

		delete packet;
		packet = nullptr;
	}
}

//singleton class
USBSender::USBSender()
{
	static bool wasInit = false;
	ASSERT(wasInit == false);
	wasInit = true;

	/* USB_HP_CAN1_TX_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(USB_HP_CAN1_TX_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USB_HP_CAN1_TX_IRQn);
	/* USB_LP_CAN1_RX0_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(USB_LP_CAN1_RX0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(USB_LP_CAN1_RX0_IRQn);

	msgQ = xQueueCreate( 2000, sizeof(USBPacket) );
	ASSERT(msgQ != NULL);
	OS::addTask( &USBSender::usbSender_deamon, "USBSen", 512, (void*)this, 0);
}

USBSender::~USBSender()
{

}

//return true on success
bool USBSender::pushData(USBPacket* packet)
{
	return xQueueSend(msgQ, (void*)&packet, (TickType_t)0) == pdTRUE;
}





