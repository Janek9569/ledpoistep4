#include <SPI/spi.hpp>
#include "global_utils.h"
#include <string.h>

/* SPI1 init function */
void SPI::MX_SPI1_Init(void)
{
	spi_handle.Instance = SPI1;
	spi_handle.Init.Mode = SPI_MODE_MASTER;
	spi_handle.Init.Direction = SPI_DIRECTION_2LINES;
	spi_handle.Init.DataSize = SPI_DATASIZE_8BIT;
	spi_handle.Init.CLKPolarity = SPI_POLARITY_LOW;
	spi_handle.Init.CLKPhase = SPI_PHASE_1EDGE;
	spi_handle.Init.NSS = SPI_NSS_SOFT;
	spi_handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	spi_handle.Init.FirstBit = SPI_FIRSTBIT_MSB;
	spi_handle.Init.TIMode = SPI_TIMODE_DISABLE;
	spi_handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	spi_handle.Init.CRCPolynomial = 10;
	ASSERT(HAL_SPI_Init(&spi_handle) == HAL_OK);
}
/* SPI2 init function */
void SPI::MX_SPI2_Init(void)
{
	spi_handle.Instance = SPI2;
	spi_handle.Init.Mode = SPI_MODE_MASTER;
	spi_handle.Init.Direction = SPI_DIRECTION_2LINES;
	spi_handle.Init.DataSize = SPI_DATASIZE_8BIT;
	spi_handle.Init.CLKPolarity = SPI_POLARITY_LOW;
	spi_handle.Init.CLKPhase = SPI_PHASE_1EDGE;
	spi_handle.Init.NSS = SPI_NSS_SOFT;
	spi_handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	spi_handle.Init.FirstBit = SPI_FIRSTBIT_MSB;
	spi_handle.Init.TIMode = SPI_TIMODE_DISABLE;
	spi_handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	spi_handle.Init.CRCPolynomial = 10;
	ASSERT(HAL_SPI_Init(&spi_handle) == HAL_OK);

	spi_dma_rx.Instance = DMA1_Channel4;
	spi_dma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	spi_dma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	spi_dma_rx.Init.MemInc = DMA_MINC_ENABLE;
	spi_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	spi_dma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	spi_dma_rx.Init.Mode = DMA_NORMAL;
	spi_dma_rx.Init.Priority = DMA_PRIORITY_LOW;
	ASSERT(HAL_DMA_Init(&spi_dma_rx) == HAL_OK);

	__HAL_LINKDMA(&spi_handle, hdmarx, spi_dma_rx);

	spi_dma_tx.Instance = DMA1_Channel5;
	spi_dma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	spi_dma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	spi_dma_tx.Init.MemInc = DMA_MINC_ENABLE;
	spi_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	spi_dma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	spi_dma_tx.Init.Mode = DMA_NORMAL;
	spi_dma_tx.Init.Priority = DMA_PRIORITY_LOW;
	ASSERT(HAL_DMA_Init(&spi_dma_tx) == HAL_OK);

	__HAL_LINKDMA(&spi_handle, hdmatx, spi_dma_tx);
}

bool SPI::isDMAEnabled() const
{
	if(spi_dma_rx.Instance != NULL && spi_dma_tx.Instance != NULL)
		return true;
	else
		return false;
}

SPI::SPI(SpiType spiType)
{
	memset(&spi_handle, 0x00, sizeof(SPI_HandleTypeDef));
	memset(&spi_dma_rx, 0x00, sizeof(DMA_HandleTypeDef));
	memset(&spi_dma_tx, 0x00, sizeof(DMA_HandleTypeDef));
	GPIO_InitTypeDef GPIO_InitStruct;
	spi_dma_rx.Instance = NULL;
	spi_dma_tx.Instance = NULL;
	switch(spiType)
	{
	case SPI_TYPE_1:
		/* SCK PA5, MISO PA6, MOSI PA7, CS PA4 */
		__HAL_RCC_GPIOA_CLK_ENABLE();

		/* NO DMA */
		MX_SPI1_Init();

		/* Init CS pin - LOW active */
		HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_SET);
		/*Configure GPIO pin : PA4 */
		GPIO_InitStruct.Pin = GP(4);
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		activateCS = []()->void {
			HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_RESET);
		};
		deactivateCS = []()->void {
			HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_SET);
		};

		attachToISR(Driver::ISR::SPI1_IRQ, [this]()->void {
			HAL_SPI_IRQHandler(&spi_handle);
			/* TODO: check if necessary */
			//i2c_handle.State = HAL_I2C_STATE_READY;
			releaseSemISR();
		});

		/* Toggle CS */
		HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, GP(4), GPIO_PIN_SET);

		releaseSem();
		break;
	case SPI_TYPE_2:
		/* SCK PB13, MISO PB14, MOSI PB15, CS PB12 */
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		/* DMA */
		MX_SPI2_Init();

		/* Init CS pin - LOW active */
		HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_SET);
		/*Configure GPIO pins : PB0 PB12 PB3 */
		GPIO_InitStruct.Pin = GP(12);
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
		activateCS = []()->void {
			HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_RESET);
		};
		deactivateCS = []()->void {
			HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_SET);
		};

		__HAL_RCC_DMA1_CLK_ENABLE();
		/* Attach ISR callbacks */
		attachToISR(Driver::ISR::DMA1_CH5, [this]()->void {
			HAL_DMA_IRQHandler(&spi_dma_tx);
			/* TODO: check if necessary */
			//i2c_handle.State = HAL_I2C_STATE_READY;
			releaseSemISR();
		});

		attachToISR(Driver::ISR::DMA1_CH4, [this]()->void {
			HAL_DMA_IRQHandler(&spi_dma_rx);
			/* TODO: check if necessary */
			//i2c_handle.State = HAL_I2C_STATE_READY;
			releaseSemISR();
		});

		attachToISR(Driver::ISR::SPI2_IRQ, [this]()->void {
			HAL_SPI_IRQHandler(&spi_handle);
			/* TODO: check if necessary */
			//i2c_handle.State = HAL_I2C_STATE_READY;
			releaseSemISR();
		});

		/* Toggle CS */
		HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GP(12), GPIO_PIN_SET);
		break;
	default:
		/* Unhandled state */
		ASSERT(1 == 0);
	}
	this->spiType = spiType;
}

SPI::~SPI()
{
	HAL_SPI_DeInit(&spi_handle);

	switch(spiType)
	{
	case SPI_TYPE_1:
		/* Deinit CS */
		HAL_GPIO_DeInit(GPIOA, GP(4));
		break;
	case SPI_TYPE_2:
		/* Deinit CS */
		HAL_GPIO_DeInit(GPIOB, GP(12));
		spi_dma_rx.Instance = NULL;
		spi_dma_tx.Instance = NULL;
		break;
	default:
		/* Unhandled state */
		ASSERT(1 == 0);
	}
	spiType = SPI_TYPE_LAST;
}

void SPI::write(uint8_t* data, uint16_t len)
{
	if(!len)
		return;

	lockSem(WAIT_FOR_EVER);
	if(isDMAEnabled())
	{
		while(HAL_SPI_Transmit_DMA(&spi_handle, data, len) != HAL_OK)
			vTaskDelay(1);
	}
	else
	{
		while(HAL_SPI_Transmit_IT(&spi_handle, data, len) != HAL_OK)
					vTaskDelay(1);
	}

	lockSem(WAIT_FOR_EVER);
	releaseSem();
}

void SPI::read(uint8_t* data, uint16_t len)
{
	if(!len)
		return;
	lockSem(WAIT_FOR_EVER);
	if(isDMAEnabled())
	{
		while(HAL_SPI_Receive_DMA(&spi_handle, data, len)!= HAL_OK)
				vTaskDelay(1);
	}
	else
	{
		while(HAL_SPI_Receive_IT(&spi_handle, data, len)!= HAL_OK)
				vTaskDelay(1);
	}

	lockSem(WAIT_FOR_EVER);
	releaseSem();
}

void SPI::writeRead(uint8_t* dataWrite, uint8_t* dataRead, uint16_t len)
{
	if(!len)
		return;
	uint8_t* writePtr = NULL;
	if(dataWrite == NULL)
	{
		writePtr = dataRead;
	}
	else
	{
		writePtr = dataWrite;
	}
	lockSem(WAIT_FOR_EVER);
	if(isDMAEnabled())
	{
		while(HAL_SPI_TransmitReceive_DMA(&spi_handle, writePtr, dataRead, len) != HAL_OK)
			vTaskDelay(1);
	}
	else
	{
		while(HAL_SPI_TransmitReceive_IT(&spi_handle, writePtr, dataRead, len) != HAL_OK)
					vTaskDelay(1);
	}

	lockSem(WAIT_FOR_EVER);
	releaseSem();
}

void SPI::readBlocking(uint8_t* data, uint16_t len, uint32_t timeout)
{
	if(!len)
		return;
	lockSem(WAIT_FOR_EVER);
	HAL_SPI_Receive(&spi_handle, data, len, timeout);
	releaseSem();
}

void SPI::writeBlocking(uint8_t* data, uint16_t len, uint32_t timeout)
{
	if(!len)
		return;
	lockSem(WAIT_FOR_EVER);
	HAL_SPI_Transmit(&spi_handle, data, len, timeout);
	releaseSem();
}

void SPI::writeReadBlocking(uint8_t* dataWrite, uint8_t* dataRead, uint16_t len, uint32_t timeout)
{
	if(!len)
		return;
	uint8_t* writePtr = NULL;
	if(dataWrite == NULL)
	{
		writePtr = dataRead;
	}
	else
	{
		writePtr = dataWrite;
	}
	lockSem(WAIT_FOR_EVER);
	HAL_SPI_TransmitReceive(&spi_handle, writePtr, dataRead, len, timeout);
	releaseSem();
}

void SPI::write(uint8_t data)
{
	write(&data, 1);
}

void SPI::read(uint8_t* data)
{
	read(data, 1);
}

void SPI::writeRead(uint8_t dataWrite, uint8_t* dataRead)
{
	writeRead(&dataWrite, dataRead, 1);
}

void SPI::writeBlocking(uint8_t data, uint32_t timeout)
{
	writeBlocking(&data, 1, timeout);
}

void SPI::readBlocking(uint8_t* data, uint32_t timeout)
{
	readBlocking(data, 1, timeout);
}

void SPI::writeReadBlocking(uint8_t dataWrite, uint8_t* dataRead, uint32_t timeout)
{
	writeReadBlocking(&dataWrite, dataRead, 1, timeout);
}



