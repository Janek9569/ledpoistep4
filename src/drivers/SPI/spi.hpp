#ifndef __SPI_H
#define __SPI_H

#include "global_utils.h"
#include "stm32f1xx_hal.h"
#include "Driver.hpp"

enum SpiType
{
	SPI_TYPE_1,
	SPI_TYPE_2,
	SPI_TYPE_LAST
};

class SPI : private Driver
{
public:
	SPI(SpiType spiType);
	~SPI();
	void write(uint8_t* data, uint16_t len);
	void read(uint8_t* data, uint16_t len);
	void writeRead(uint8_t* dataWrite, uint8_t* dataRead, uint16_t len);

	void readBlocking(uint8_t* data, uint16_t len, uint32_t timeout);
	void writeBlocking(uint8_t* data, uint16_t len, uint32_t timeout);
	void writeReadBlocking(uint8_t* dataWrite, uint8_t* dataRead, uint16_t len, uint32_t timeout);

	void write(uint8_t data);
	void read(uint8_t* data);
	void writeRead(uint8_t dataWrite, uint8_t* dataRead);

	void writeBlocking(uint8_t data, uint32_t timeout);
	void readBlocking(uint8_t* data, uint32_t timeout);
	void writeReadBlocking(uint8_t dataWrite, uint8_t* dataRead, uint32_t timeout);

	std::function<void(void)> activateCS, deactivateCS;
private:
	void MX_SPI1_Init(void);
	void MX_SPI2_Init(void);
	bool isDMAEnabled() const;

	SPI_HandleTypeDef spi_handle;
	DMA_HandleTypeDef spi_dma_rx;
	DMA_HandleTypeDef spi_dma_tx;
	SpiType spiType;
};

#endif
