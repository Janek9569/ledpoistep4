#include "spi.hpp"
#include "clock.h"
#include "OS.hpp"
#include "global_utils.h"
#include "USBSender.h"
#include "spi.hpp"
#include "M25P32.hpp"
#include "spiffs_initialiser.hpp"
#include "WS2812B.hpp"
#include "ImageVisualizer.h"
#include "usb_device.h"

#include "main.h"
#include "stm32f1xx_hal.h"
#include "gpio.h"
#include <string.h>

#include "images.hpp"

struct ImageInfo
{
	const char* name;
};

ImageInfo imageNames[] =
{
	"stars1.bin",
	"stars2.bin",
	"stars3.bin",
	"stars4.bin",
	"stars5.bin"
};

uint8_t * Wheel(uint8_t WheelPos);
void rainbowCycle(int SpeedDelay, WS2812B* ws) {
	static Pix pixs[100];
  uint8_t *c;
  uint16_t i, j;
  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< 100; i++) {
      c=Wheel(((i * 256 / 100) + j) & 255);
      Pix pix;
      pix.R = *c;
      pix.G = *(c+1);
      pix.B = *(c+2);
      pixs[i] = pix;
    }
    ws->setRow(pixs);
    vTaskDelay(SpeedDelay);
  }
}

uint8_t * Wheel(uint8_t WheelPos) {
  static uint8_t c[3];
  if(WheelPos < 85) {
   c[0]=WheelPos * 3;
   c[1]=255 - WheelPos * 3;
   c[2]=0;
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   c[0]=255 - WheelPos * 3;
   c[1]=0;
   c[2]=WheelPos * 3;
  } else {
   WheelPos -= 170;
   c[0]=0;
   c[1]=WheelPos * 3;
   c[2]=255 - WheelPos * 3;
  }
  return c;
}

static void resourceAllocator(void* argument)
{
	MX_GPIO_Init();
	WS2812B *ws = new WS2812B(100, TIM1_CH1);
	ws->setContrast(6);
	while(true)
	{
		rainbowCycle(15, ws);
	}



	/*
	spiffs_init();
	uint32_t actImgNum = 0;

	MX_GPIO_Init();
	ImageVisualizer viz;
	for(;;)
	{
		ImageAnimationParam par;
		sprintf(par.name, "%s", imageNames[actImgNum].name);
		++actImgNum;

		if(actImgNum >= 5)
		{
			actImgNum = 0;
		}

		viz.pushImage(&par);
	}*/
}

int main(void)
{
	HAL_Init();
	SystemClock_Config();
	OS::addTask(resourceAllocator, "RES_ALLOC", 256, (void*)0, 1);
	OS::startScheduler();
	return 0;
}

