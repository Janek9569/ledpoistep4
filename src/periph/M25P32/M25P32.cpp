#include "M25P32.hpp"

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

M25P32::StatusRegister M25P32::readStatusRegister()
{
	M25P32::StatusRegister statusRegister = { 0 };
	writeCommandCR(READ_STATUS_REGISTER, (uint8_t*) &statusRegister, sizeof(statusRegister));
	return statusRegister;
}

/* command + read + write */
void M25P32::writeCommandCRW(M25P32::M25P32Commands command, uint8_t* writeData, uint32_t writeDataLen,
		uint8_t* readData, uint32_t readDataLen)
{
	spi.activateCS();
	spi.write(command);
	spi.write(writeData, writeDataLen);
	spi.writeRead(NULL, readData, readDataLen);
	spi.deactivateCS();
}

/* command + write */
void M25P32::writeCommandCW(M25P32::M25P32Commands command, const uint8_t* writeData, uint32_t writeDataLen)
{
	spi.activateCS();
	spi.write(command);
	spi.write( (uint8_t*)writeData, writeDataLen);
	spi.deactivateCS();
}

void M25P32::writeCommandCW(M25P32::M25P32Commands command, const uint8_t* writeData1, uint32_t writeDataLen1,
		const uint8_t* writeData2, uint32_t writeDataLen2)
{
	spi.activateCS();
	spi.write(command);
	spi.write( (uint8_t*)writeData1, writeDataLen1);
	spi.write( (uint8_t*)writeData2, writeDataLen2);
	spi.deactivateCS();
}

/* command + read */
void M25P32::writeCommandCR(M25P32::M25P32Commands command, uint8_t* readData, uint32_t readDataLen)
{
	spi.activateCS();
	spi.write(command);
	spi.writeRead(NULL, readData, readDataLen);
	spi.deactivateCS();
}

/* command */
void M25P32::writeCommandC(M25P32::M25P32Commands command)
{
	spi.activateCS();
	spi.write(command);
	spi.deactivateCS();
}

void M25P32::waitUntilDevNotBusy()
{
	while(readStatusRegister().WIP == 1)
		vTaskDelay(1);
}

void M25P32::waitForWriteEnable()
{
	while(readStatusRegister().WEL != 1)
			vTaskDelay(1);
}

M25P32::M25P32():
	spi(SPI_TYPE_1)
{
	spi.deactivateCS();

	GPIO_InitTypeDef GPIO_InitStruct;
	/* set W# pin to LOW - PB0 */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GP(0), GPIO_PIN_RESET);
	/*Configure GPIO pins : PB0 PB12 PB3 */
	GPIO_InitStruct.Pin = GP(0);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* Wake from powerdown */
	waitUntilDevNotBusy();
	writeCommandC(RELEASE_from_DEEP_POWER_DOWN);
	vTaskDelay(5);

	/* Read identification */
	waitUntilDevNotBusy();
	writeCommandCR(READ_IDENTIFICATION_1, (uint8_t*) &identif, sizeof(identif));

	/* Assert identification */
	ASSERT(identif.manufactID == 0x20);
	ASSERT(identif.memoryType == 0x20);
	ASSERT(identif.memoryCapa == 0x16);

	/* Clear protected block number */
	/* Write enable */
	M25P32::StatusRegister statusRegister = { 0 };
	statusRegister.WEL = 1;
	waitUntilDevNotBusy();
	writeCommandC(WRITE_ENABLE);
	waitUntilDevNotBusy();
	waitForWriteEnable();
	writeCommandCW(WRITE_STATUS_REGISTER, (uint8_t*)&statusRegister, sizeof(statusRegister));
	waitUntilDevNotBusy();

	/* Read status register */
	statusRegister = readStatusRegister();
	//ASSERT(statusRegister.SRWD == 0); //disabled write protection
	//ASSERT(statusRegister.blockProtect == 0); //no protected block

	/* Write disable */
	writeCommandC(WRITE_DISABLE);
}

M25P32::~M25P32()
{

}

int32_t M25P32::write(uint32_t addr, uint32_t size, uint8_t *src)
{
	ASSERT(addr + size <= MAX_ADDR);
	uint8_t* data = (uint8_t*)src;
	size_t leftBytes = size;
	uint16_t maxBytes = PAGE_SIZE - (addr % PAGE_SIZE);
	uint32_t offset = 0;

	/* Write pages */
	while(leftBytes)
	{
		uint32_t actSize = MIN(leftBytes, maxBytes);
		uint8_t actAddr[] =
		{
			(uint8_t)(addr >> 16),
			(uint8_t)(addr >> 8),
			(uint8_t)(addr)
		};

		/* Write enable */
		waitUntilDevNotBusy();
		writeCommandC(WRITE_ENABLE);
		waitForWriteEnable();

		/* Program data */
		writeCommandCW(PAGE_PROGRAM, actAddr, 3, data + offset, actSize);

		offset += actSize;
		addr += actSize;
		leftBytes-= actSize;
		maxBytes = PAGE_SIZE;
	}
	return 0;
}

int32_t M25P32::read(uint32_t addr, uint32_t size, uint8_t *dst)
{
	ASSERT(addr + size <= MAX_ADDR);
	waitUntilDevNotBusy();

	uint8_t actAddr[] =
	{
		(uint8_t)(addr >> 16L),
		(uint8_t)(addr >> 8L),
		(uint8_t)(addr),
		0
	};
	writeCommandCRW(READ_DATA_BYTES_at_HIGHER_SPEED, actAddr, 4, (uint8_t*) dst, size);
	return 0;
}

void M25P32::format()
{
	waitUntilDevNotBusy();
	writeCommandC(WRITE_ENABLE);
	waitForWriteEnable();
	writeCommandC(BULK_ERASE);
}

int32_t M25P32::erase(uint32_t addr, uint32_t size)
{
	ASSERT(size%SECTOR_SIZE == 0);

	for(uint32_t i = 0; i < size/SECTOR_SIZE; ++i)
	{
		waitUntilDevNotBusy();
		writeCommandC(WRITE_ENABLE);
		waitForWriteEnable();
		uint32_t sectorAddr = addr + i * SECTOR_SIZE;
		uint8_t actAddr[] =
		{
			(uint8_t)(sectorAddr >> 16L),
			(uint8_t)(sectorAddr >> 8L),
			(uint8_t)(sectorAddr),
		};
		writeCommandCW(SECTOR_ERASE, actAddr, 3);
	}
	return 0;
}
