#ifndef M25P32_HPP_
#define M25P32_HPP_

#include "spi.hpp"

class M25P32
{
public:
	M25P32();
	~M25P32();

	int32_t write(uint32_t addr, uint32_t size, uint8_t *src);
	int32_t read(uint32_t addr, uint32_t size, uint8_t *dst);
	int32_t erase(uint32_t addr, uint32_t size);
	void format();

private:
	enum M25P32Commands
	{
		WRITE_ENABLE = 0x06,
		WRITE_DISABLE = 0x04,
		READ_IDENTIFICATION_1 = 0x9F,
		READ_IDENTIFICATION_2 = 0x9E,
		READ_STATUS_REGISTER = 0x05,
		WRITE_STATUS_REGISTER = 0x01,
		READ_DATA_BYTES = 0x03,
		READ_DATA_BYTES_at_HIGHER_SPEED = 0x0B,
		PAGE_PROGRAM = 0x02,
		SECTOR_ERASE = 0xD8,
		BULK_ERASE = 0xC7,
		DEEP_POWER_DOWN = 0xB9,
		RELEASE_from_DEEP_POWER_DOWN = 0xAB,
		READ_ELECTRONIC_SIGNATURE_and_RELEASE_from_DEEP_POWER_DOWN = 0xAB
	};

	struct __attribute__((packed)) Identif
	{
		uint8_t manufactID;
		uint8_t memoryType;
		uint8_t memoryCapa;
	};

	struct __attribute__((packed)) StatusRegister
	{
		unsigned int WIP:1;
		unsigned int WEL:1;
		unsigned int blockProtect:3;
		unsigned int dummy:2;
		unsigned int SRWD:1;
	};

	SPI spi;
	Identif identif;

	static constexpr uint8_t SECTOR_COUNT = 64;
	static constexpr uint32_t MAX_ADDR = 0x003FFFFF;
	static constexpr uint32_t SECTOR_SIZE = 0xFFFF;
	static constexpr uint32_t PAGE_SIZE = 256;
	static constexpr uint32_t PAGES_IN_SECTOR = 256;

	StatusRegister readStatusRegister();
	void writeCommandCRW(M25P32::M25P32Commands command, uint8_t* writeData, uint32_t writeDataLen,
			uint8_t* readData, uint32_t readDataLen);
	void writeCommandCR(M25P32::M25P32Commands command, uint8_t* readData, uint32_t readDataLen);
	void writeCommandCW(M25P32::M25P32Commands command, const uint8_t* writeData, uint32_t writeDataLen);
	void writeCommandCW(M25P32::M25P32Commands command, const uint8_t* writeData1, uint32_t writeDataLen1,
			const uint8_t* writeData2, uint32_t writeDataLen2);
	void writeCommandC(M25P32::M25P32Commands command);
	void waitUntilDevNotBusy();
	void waitForWriteEnable();
};

#endif
