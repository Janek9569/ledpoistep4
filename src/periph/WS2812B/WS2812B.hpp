#ifndef PERIPH_WS2812B_WS2812B_HPP_
#define PERIPH_WS2812B_WS2812B_HPP_

#include <stdint.h>
#include "PWM.hpp"

struct Pix
{
	uint8_t R,G,B;
};

class WS2812B
{
public:
	enum Mode
	{
		MULTICOLOUR
	};

	WS2812B(uint32_t ledCount, PWMType type);
	WS2812B(uint32_t ledCount, PWMType type, uint8_t** allocatedBuffers);
	~WS2812B();

	void setMode(Mode mode) { this->mode = mode; }
	void setContrast(uint8_t contrast) { this->contrast = contrast; }
	void latchBuffer();
	void setRow(Pix* row);
	void setLED(uint8_t ledID, Pix pix);
	uint8_t** getBuffers() { return commBuffer; }

	void* operator new(size_t size);
	void operator delete(void* p);
private:
	Mode mode;
	PWM pwm;
	uint8_t contrast;
	uint32_t ledCount;
	uint32_t bufferLen;
	uint8_t* commBuffer[2];
	uint8_t actBufferLatched;

	static constexpr uint8_t ONE_VALUE = 58;
	static constexpr uint8_t ZERO_VALUE = 24;
	static constexpr uint8_t RST_VALUE = 0;
	static constexpr uint8_t RESET_LEN = 50;

	uint8_t* getBackBuffer();
	void toggleBackBufferNum();
};


#endif
