#include "WS2812B.hpp"
#include "string.h"
#include "limits.h"
#include "FreeRTOS.h"
#include "global_utils.h"

WS2812B::WS2812B(uint32_t ledCount, PWMType type):
	pwm(type)
{
	this->actBufferLatched = 0;
	this->ledCount = ledCount;
	this->bufferLen = ledCount*8*3 + RESET_LEN;
	this->contrast = UINT8_MAX;
	this->commBuffer[0] = (uint8_t*) pvPortMalloc(this->bufferLen * sizeof(uint8_t));
	this->commBuffer[1] = (uint8_t*) pvPortMalloc(this->bufferLen * sizeof(uint8_t));

	ASSERT(this->commBuffer[0] != NULL);
	ASSERT(this->commBuffer[1] != NULL);

	for(uint32_t i = this->bufferLen - RESET_LEN; i < this->bufferLen; ++i)
	{
		this->commBuffer[0][i] = RST_VALUE;
		this->commBuffer[1][i] = RST_VALUE;
	}
	for(uint32_t i=0; i < this->bufferLen - RESET_LEN; ++i)
	{
		this->commBuffer[0][i] = ZERO_VALUE;
		this->commBuffer[1][i] = ZERO_VALUE;
	}
}

WS2812B::WS2812B(uint32_t ledCount, PWMType type, uint8_t** allocatedBuffers):
	pwm(type)
{
	this->actBufferLatched = 0;
	this->ledCount = ledCount;
	this->bufferLen = ledCount*8*3 + RESET_LEN;
	this->contrast = UINT8_MAX;
	this->commBuffer[0] = allocatedBuffers[0];
	this->commBuffer[1] = allocatedBuffers[1];

	ASSERT(this->commBuffer[0] != NULL);
	ASSERT(this->commBuffer[1] != NULL);

	for(uint32_t i = this->bufferLen - RESET_LEN; i < this->bufferLen; ++i)
	{
		this->commBuffer[0][i] = RST_VALUE;
		this->commBuffer[1][i] = RST_VALUE;
	}
	for(uint32_t i=0; i < this->bufferLen - RESET_LEN; ++i)
	{
		this->commBuffer[0][i] = ZERO_VALUE;
		this->commBuffer[1][i] = ZERO_VALUE;
	}
}

WS2812B::~WS2812B()
{
	if(this->commBuffer[0])
	{
		vPortFree(this->commBuffer[0]);
	}
	if(this->commBuffer[1])
	{
		vPortFree(this->commBuffer[1]);
	}
}

void WS2812B::latchBuffer()
{
	uint8_t* data = getBackBuffer();
	pwm.transmitStream(data, bufferLen);
	toggleBackBufferNum();
}

void WS2812B::setRow(Pix* row)
{
	for(uint32_t i = 0; i < ledCount; ++i)
	{
		setLED(i, row[i]);
	}
	latchBuffer();
}

void WS2812B::setLED(uint8_t ledID, Pix pix)
{
	ASSERT(ledID <= ledCount);
	ASSERT(contrast > 0);
	uint32_t i;
	uint32_t j;
	uint8_t col[3];
	col[0] = pix.G/contrast;
	col[1] = pix.R/contrast;
	col[2] = pix.B/contrast;
	uint8_t* data = getBackBuffer();

	// BY RED GREEN BLUE
	for(i=0; i<3; ++i)
	{
		// BY 8 bits per color
		for(j=0; j<8; ++j)
		{
			if(col[i] & (0x80>>j))
				data[24*ledID+(i*8)+j] = ONE_VALUE;
			else
				data[24*ledID+(i*8)+j] = ZERO_VALUE;
		}
	}
}

uint8_t* WS2812B::getBackBuffer()
{
	uint8_t backBufferNum = actBufferLatched == 0 ? 1 : 0;
	return commBuffer[backBufferNum];
}

void WS2812B::toggleBackBufferNum()
{
	if(actBufferLatched == 0)
		actBufferLatched = 1;
	else
		actBufferLatched = 0;
}

void* WS2812B::operator new(size_t size)
{
	void* p = pvPortMalloc(size);
	return p;
}

void WS2812B::operator delete(void* p)
{
	vPortFree(p);
}
