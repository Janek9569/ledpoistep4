#include "FreeRTOS.h"
#include <string.h>

void* lodepng_malloc(size_t size)
{
	  return pvPortMalloc(size);
}

void* lodepng_realloc(void* ptr, size_t new_size)
{
	if(!ptr)
	{
		return pvPortMalloc(new_size);
	}
	else
	{
		uint32_t size = vPortMallocSize(ptr);
		void* newData = pvPortMalloc(new_size);

		if(newData == NULL)
			return NULL;

		if(size)
			memcpy(newData, ptr, size);
		vPortFree(ptr);
		return newData;
	}

	return NULL;
}

void lodepng_free(void* ptr)
{
	vPortFree(ptr);
}

