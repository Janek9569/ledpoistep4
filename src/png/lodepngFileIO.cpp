#include "spiffs.h"
#include "spiffs_initialiser.hpp"
#include "limits.h"
#include "lodepng.hpp"

void* lodepng_malloc(size_t size);
void* lodepng_realloc(void* ptr, size_t new_size);
void lodepng_free(void* ptr);

/* returns negative value on error. This should be pure C compatible, so no fstat. */
long lodepng_filesize(const char* filename)
{
	spiffs_file file;
	spiffs_stat stat;
	file = SPIFFS_open(&fs, filename, SPIFFS_RDONLY, 0777);
	if(file < 0)
		return -1;

	if(SPIFFS_fstat(&fs, file, &stat) < 0)
	{
		SPIFFS_close(&fs, file);
		return -1;
	}

	if(stat.size == LONG_MAX)
		stat.size = -1;
	else
		stat.size = stat.size;

	SPIFFS_close(&fs, file);
	return stat.size;
}

/* load file into buffer that already has the correct allocated size. Returns error code.*/
unsigned lodepng_buffer_file(unsigned char* out, size_t size, const char* filename)
{
	spiffs_file file;
	size_t readsize;

	file = SPIFFS_open(&fs, filename, SPIFFS_RDONLY, 0777);

	if(file < 0)
		return 78;

	readsize = SPIFFS_read(&fs, file, out, size);
	SPIFFS_close(&fs, file);

	if (readsize != size)
		return 78;
	return 0;
}

unsigned lodepng_load_file(unsigned char** out, size_t* outsize, const char* filename)
{
	long size = lodepng_filesize(filename);
	if (size < 0)
	  return 78;

	*outsize = (size_t)size;

	*out = (unsigned char*)lodepng_malloc((size_t)size);
	if(!(*out) && size > 0)
	  return 83; /*the above malloc failed*/

	return lodepng_buffer_file(*out, (size_t)size, filename);
}

/*write given buffer to the file, overwriting the file, it doesn't append to it.*/
unsigned lodepng_save_file(const unsigned char* buffer, size_t buffersize, const char* filename)
{
	spiffs_file file;
	int32_t written = 0;
	file = SPIFFS_open(&fs, filename, SPIFFS_WRONLY, 0777);

	if(file < 0)
		return 79;

	written = SPIFFS_write(&fs, file, (void*)buffer, buffersize);

	SPIFFS_close(&fs, file);

	if(written < 0)
		return 79;

	if((size_t)written < buffersize)
		return 79;

	return 0;
}

unsigned lodepng_decode_file(unsigned char** out, unsigned* w, unsigned* h, const char* filename,
                             LodePNGColorType colortype, unsigned bitdepth)
{
  unsigned char* buffer = 0;
  size_t buffersize;
  unsigned error;
  error = lodepng_load_file(&buffer, &buffersize, filename);
  if(!error) error = lodepng_decode_memory(out, w, h, buffer, buffersize, colortype, bitdepth);
  lodepng_free(buffer);
  return error;
}

unsigned lodepng_decode32_file(unsigned char** out, unsigned* w, unsigned* h, const char* filename)
{
  return lodepng_decode_file(out, w, h, filename, LCT_RGBA, 8);
}

unsigned lodepng_decode24_file(unsigned char** out, unsigned* w, unsigned* h, const char* filename)
{
  return lodepng_decode_file(out, w, h, filename, LCT_RGB, 8);
}

unsigned lodepng_encode_file(const char* filename, const unsigned char* image, unsigned w, unsigned h,
                             LodePNGColorType colortype, unsigned bitdepth)
{
  unsigned char* buffer;
  size_t buffersize;
  unsigned error = lodepng_encode_memory(&buffer, &buffersize, image, w, h, colortype, bitdepth);
  if(!error) error = lodepng_save_file(buffer, buffersize, filename);
  lodepng_free(buffer);
  return error;
}

unsigned lodepng_encode32_file(const char* filename, const unsigned char* image, unsigned w, unsigned h)
{
  return lodepng_encode_file(filename, image, w, h, LCT_RGBA, 8);
}

unsigned lodepng_encode24_file(const char* filename, const unsigned char* image, unsigned w, unsigned h)
{
  return lodepng_encode_file(filename, image, w, h, LCT_RGB, 8);
}

unsigned load_file(std::vector<unsigned char>& buffer, const std::string& filename)
{
  long size = lodepng_filesize(filename.c_str());
  if(size < 0) return 78;
  buffer.resize((size_t)size);
  return size == 0 ? 0 : lodepng_buffer_file(&buffer[0], (size_t)size, filename.c_str());
}

/*write given buffer to the file, overwriting the file, it doesn't append to it.*/
unsigned save_file(const std::vector<unsigned char>& buffer, const std::string& filename)
{
  return lodepng_save_file(buffer.empty() ? 0 : &buffer[0], buffer.size(), filename.c_str());
}

unsigned decode(std::vector<unsigned char>& out, unsigned& w, unsigned& h, const std::string& filename,
                LodePNGColorType colortype, unsigned bitdepth)
{
	return -1;
}

unsigned encode(const std::string& filename,
                const unsigned char* in, unsigned w, unsigned h,
                LodePNGColorType colortype, unsigned bitdepth)
{
	return -1;
}

unsigned encode(const std::string& filename,
                const std::vector<unsigned char>& in, unsigned w, unsigned h,
                LodePNGColorType colortype, unsigned bitdepth)
{
	return -1;
}
