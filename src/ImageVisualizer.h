#ifndef IMAGEVISUALIZER_H_
#define IMAGEVISUALIZER_H_

#include "WS2812B.hpp"
#include "queue.h"

#define IMG_MAX_HEIGHT 200

class ImageAnimationParam
{
public:
	char name[20];
};

class ImageVisualizer
{
public:
	enum ShowType
	{
		Images,
		Multicolour
	};

	void setContrast(uint8_t contrast);
	bool pushImage(ImageAnimationParam* par);
	ImageVisualizer();
	~ImageVisualizer();
private:
	enum LedCol
	{
		BACK1 = 0,
		BACK2 = 1,
		FRONT2 = 2,
		FRONT1 = 3,
		LAST
	};

	static bool imageVisualizerCounter;
	static constexpr uint8_t WS_DRIVERS = 4;
	static constexpr uint8_t WS_ROW_WIDTH = 100;
	static constexpr uint8_t MIN_CONTRAST = 8;
	QueueHandle_t imageQ;

	uint8_t* image;
	unsigned imgWidth, imgHeight;
	uint32_t actColumn;

	WS2812B* ws[WS_DRIVERS];

	static void imageSneder(void* argument);
	void updateImage();
};

#endif
