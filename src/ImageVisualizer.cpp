#include <ImageVisualizer.h>
#include "OS.hpp"
#include "global_utils.h"
#include "lodepng.hpp"
#include "spiffs_initialiser.hpp"
#define IMG_Q_SIZE 10

bool ImageVisualizer::imageVisualizerCounter = false;
//0-back of 1, 1-back of 2, 2-front of 2, 3-front of 1
ImageVisualizer::ImageVisualizer()
{
	ASSERT(imageVisualizerCounter == 0);
	imageQ = xQueueCreate(IMG_Q_SIZE, sizeof(ImageAnimationParam));
	ASSERT(imageQ != NULL);

	ws[0] = new WS2812B(100, TIM1_CH1);
	//ws[1] = new WS2812B(100, TIM1_CH2);
	//ws[2] = new WS2812B(100, TIM1_CH3, ws[1]->getBuffers());
	//ws[3] = new WS2812B(100, TIM2_CH2_4);
	setContrast(MIN_CONTRAST);

	image = NULL;
	imgWidth = 0;
	imgHeight = 0;
	actColumn = 0;

	OS::addTask(imageSneder, "IMG_SEND", 256, (void*)this, 0);

	imageVisualizerCounter = true;
}

ImageVisualizer::~ImageVisualizer()
{
	imageVisualizerCounter = false;
}

void ImageVisualizer::updateImage()
{
	ASSERT(imgHeight < IMG_MAX_HEIGHT);

	for(uint32_t x = 0; x < imgWidth; ++x)
	{
		uint8_t r, g, b;

		r = image[4 * actColumn * imgWidth + 4 * x + 0];
		g = image[4 * actColumn * imgWidth + 4 * x + 1];
		b = image[4 * actColumn * imgWidth + 4 * x + 2];

		Pix pix =
		{
			.R = r,
			.G = g,
			.B = b
		};

		if(x > WS_ROW_WIDTH)
		{
			ws[BACK1]->setLED(x, pix);
			ws[FRONT1]->setLED(x, pix);
		}
		else
		{
			//ws[BACK2]->setLED(x - WS_ROW_WIDTH, pix);
		}
	}
}
void ImageVisualizer::imageSneder(void* argument)
{
	static Pix colBuffer[100] = { 0 };
	ImageVisualizer* vis = (ImageVisualizer*)argument;
	ImageAnimationParam param;
	while(true)
	{
		if(xQueueReceive(vis->imageQ, &(param), 0))
		{
			vis->image = NULL;

			spiffs_file fd = SPIFFS_open(&fs, param.name, SPIFFS_RDONLY, 0);
			ASSERT(fd >= 0);
			uint8_t headerBuffer[3*sizeof(unsigned int)] = {0};

			ASSERT(SPIFFS_read(&fs, fd, (u8_t *)headerBuffer, 3*sizeof(unsigned int)) >= 0);
			unsigned int height = *((uint32_t*) (headerBuffer + sizeof(unsigned int)));

			while(height--)
			{
				ASSERT(SPIFFS_read(&fs, fd, (u8_t *)colBuffer, 100*sizeof(Pix)) >= 0);
				vis->ws[BACK1]->setRow(colBuffer);
				//vis->ws[FRONT1]->setRow(colBuffer);
				vTaskDelay(1);
			}
			ASSERT(SPIFFS_close(&fs, fd) >= 0);
		}
	}
}
/*
void ImageVisualizer::imageSneder(void* argument)
{
	ImageVisualizer* vis = (ImageVisualizer*)argument;
	ImageAnimationParam param;
	uint8_t actSlice = 0;
	char nameBuffer[50] = { 0 };
	while(true)
	{
		if(xQueueReceive(vis->imageQ, &(param), 0))
		{
			while(param.time)
			{
				vPortFree(vis->image);
				vis->image = NULL;

				sprintf(nameBuffer, "%s_%d.png", param.name, actSlice);
				if(lodepng_decode32_file(&vis->image, &vis->imgWidth, &vis->imgHeight, nameBuffer))
					vis->image = NULL;

				for(uint32_t i = 0; i < vis->imgWidth; ++i)
				{
					if(vis->image)
						vis->updateImage();

					vTaskDelay(1);
					--param.time;
					if(param.time == 0)
						break;
				}
				++actSlice;
				if(actSlice > param.slices)
					actSlice = 0;
			}
		}
		else
		{
			vPortFree(vis->image);
			vis->image = NULL;

			sprintf(nameBuffer, "%s_%d", param.name, actSlice);
			if(lodepng_decode32_file(&vis->image, &vis->imgWidth, &vis->imgHeight, param.name))
				vis->image = NULL;

			for(uint32_t i = 0; i < vis->imgWidth; ++i)
			{
				if(vis->image)
					vis->updateImage();

				vTaskDelay(1);
				--param.time;
				if(param.time == 0)
					break;
			}
			++actSlice;
			if(actSlice > param.slices)
				actSlice = 0;
		}
	}
}*/

void ImageVisualizer::setContrast(uint8_t contrast)
{
	ASSERT(contrast >= MIN_CONTRAST);
	ws[BACK1]->setContrast(contrast);
	//ws[FRONT1]->setContrast(contrast);
	//TODO: cleanup
	/*
	for(uint8_t i = 0 ; i < LAST; ++i)
	{
		ws[i]->setContrast(contrast);
	}*/
}

bool ImageVisualizer::pushImage(ImageAnimationParam* par)
{
	if( xQueueSend( imageQ, ( void * ) par, WAIT_FOR_EVER ) != pdPASS )
	{
		return false;
	}
	return true;
}





