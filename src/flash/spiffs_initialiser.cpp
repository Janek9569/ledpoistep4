#include "spiffs_initialiser.hpp"
#include "global_utils.h"
#include "spiffs_config.h"
#include "images.hpp"

#define LOG_PAGE_SZ 256

spiffs fs;
static M25P32* _M25P32 = NULL;
static u8_t spiffs_work_buf[LOG_PAGE_SZ*2];
static u8_t spiffs_fds[32*4];
static u8_t spiffs_cache_buf[(LOG_PAGE_SZ+32)*4];

CEXT void spiffs_init()
{
	_M25P32 = new M25P32;
	spiffs_config cfg;
	cfg.hal_erase_f = spiffs_erase_cwrapper;
	cfg.hal_read_f = spiffs_read_cwrapper;
	cfg.hal_write_f = spiffs_write_cwrapper;

	int res = SPIFFS_mount(&fs, &cfg, spiffs_work_buf, spiffs_fds,
	      sizeof(spiffs_fds), spiffs_cache_buf, sizeof(spiffs_cache_buf), 0);

#ifdef STARS_BITSTREAM
	//stars1-5
	spiffs_file fd = SPIFFS_open(&fs, "stars5.bin", SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
	ASSERT(fd >= 0);
	ASSERT(SPIFFS_write(&fs, fd, (u8_t *)&stars5, sizeof(stars5)) >= 0);
	ASSERT(SPIFFS_close(&fs, fd) >= 0);
#endif

#ifdef I00
	for(int i = 0; i < 50; ++i)
	{
		static char buffer[100] = { 0 };
		sprintf(buffer, "00_%d.png", i);

		spiffs_file fd = SPIFFS_open(&fs, buffer, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
		ASSERT(fd >= 0);
		ASSERT(SPIFFS_write(&fs, fd, (u8_t *)i00_pointers[i], sizeof(i00_49)) >= 0);
		ASSERT(SPIFFS_close(&fs, fd) >= 0);
	}
#endif

#ifdef ARROWS
	for(int i = 100; i < 114; ++i)
	{
		static char buffer[100] = { 0 };
		sprintf(buffer, "arrows_%d.png", i);

		spiffs_file fd = SPIFFS_open(&fs, buffer, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
		ASSERT(fd >= 0);
		ASSERT(SPIFFS_write(&fs, fd, (u8_t *)arrows_pointers[i-100], sizeof(arrows_100)) >= 0);
		ASSERT(SPIFFS_close(&fs, fd) >= 0);
	}
#endif

#ifdef HEX
	for(int i = 0; i < 50; ++i)
	{
		static char buffer[100] = { 0 };
		sprintf(buffer, "hex_%d.png", i);

		spiffs_file fd = SPIFFS_open(&fs, buffer, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
		ASSERT(fd >= 0);
		ASSERT(SPIFFS_write(&fs, fd, (u8_t *)hex_pointers[i], sizeof(hex_0)) >= 0);
		ASSERT(SPIFFS_close(&fs, fd) >= 0);
	}
#endif

#ifdef KOLKA
	for(int i = 90; i < 100; ++i)
	{
		static char buffer[100] = { 0 };
		sprintf(buffer, "kolka_%d.png", i);

		spiffs_file fd = SPIFFS_open(&fs, buffer, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
		ASSERT(fd >= 0);
		ASSERT(SPIFFS_write(&fs, fd, (u8_t *)kolka_pointers[i-90], sizeof(kolka_90)) >= 0);
		ASSERT(SPIFFS_close(&fs, fd) >= 0);
	}
#endif

#ifdef STARS
	for(int i = 0; i < 50; ++i)
	{
		static char buffer[100] = { 0 };
		sprintf(buffer, "stars_%d.png", i);

		spiffs_file fd = SPIFFS_open(&fs, buffer, SPIFFS_CREAT | SPIFFS_TRUNC | SPIFFS_RDWR, 0);
		ASSERT(fd >= 0);
		ASSERT(SPIFFS_write(&fs, fd, (u8_t *)stars_pointers[i], sizeof(stars_0)) >= 0);
		ASSERT(SPIFFS_close(&fs, fd) >= 0);
	}
#endif

	ASSERT(res == 0);
}

CEXT int32_t spiffs_write_cwrapper(uint32_t addr, uint32_t size, uint8_t *src)
{
	return _M25P32->write(addr, size, src);
}

CEXT int32_t spiffs_read_cwrapper(uint32_t addr, uint32_t size, uint8_t *dst)
{
	return _M25P32->read(addr, size, dst);
}

CEXT int32_t spiffs_erase_cwrapper(uint32_t addr, uint32_t size)
{
	return _M25P32->erase(addr, size);
}

