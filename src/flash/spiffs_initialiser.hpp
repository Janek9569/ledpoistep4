#ifndef FLASH_SPIFFS_INITIALISER_HPP_
#define FLASH_SPIFFS_INITIALISER_HPP_

#include "spiffs.h"
#include "M25P32.hpp"

extern spiffs fs;
CEXT void spiffs_init();
CEXT int32_t spiffs_write_cwrapper(uint32_t addr, uint32_t size, uint8_t *src);
CEXT int32_t spiffs_read_cwrapper(uint32_t addr, uint32_t size, uint8_t *dst);
CEXT int32_t spiffs_erase_cwrapper(uint32_t addr, uint32_t size);

#endif
